import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogComponent } from './pages/blog/blog.component';
import { CompaniesComponent } from './pages/companies/companies.component';
import { CompanyComponent } from './pages/company/company.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CvComponent } from './pages/cv/cv.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DonationComponent } from './pages/donation/donation.component';
import { ErrorComponent } from './pages/error/error.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { PolicyComponent } from './pages/policy/policy.component';
import { ProjectComponent } from './pages/project/project.component';
import { ProjectBlackmirrorComponent } from './pages/project/specials/project-blackmirror/project-blackmirror.component';
import { ProjectDoodlesComponent } from './pages/project/specials/project-doodles/project-doodles.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { TimelineComponent } from './pages/timeline/timeline.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'timeline', component: TimelineComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'project', component: ProjectComponent},
  {path: 'blackmirror', component: ProjectBlackmirrorComponent},
  {path: 'doodles', component: ProjectDoodlesComponent},
  {path: 'companies', component: CompaniesComponent},
  {path: 'company', component: CompanyComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'donation', component: DonationComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'policy', component: PolicyComponent},
  {path: 'cv', component: CvComponent},
  {path: 'error', component: ErrorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
