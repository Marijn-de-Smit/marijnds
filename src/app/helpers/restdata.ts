import {HttpHeaders} from "@angular/common/http";
import { environment } from '../../environments/environment';

export class Restdata {

  private url = environment.serverUrl;

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*",
      'Access-Control-Allow-Credentials': 'true',
      Authorization: "Bearer " + localStorage.getItem("token")
    })
  };

  public getUrl(keyword: string): string {
    return this.url + keyword + '/';
  }

}
