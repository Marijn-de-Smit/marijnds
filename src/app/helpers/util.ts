import { HttpErrorResponse } from "@angular/common/http";

const NEW_LINE = '\n';

export class Util {
    static TIME_FROM = "00:00";
    static TIME_TO = "23:59";

    public static createErrorMessage(error: Response): string {
        let message = "Algemene fout";
        if (error instanceof HttpErrorResponse) {
            if (error) {
                message = "De aanvraag kon niet worden verwerkt, de service is niet beschikbaar (404).";
            }
        }

        return message;
    }
}