import { CompanyType } from '../enums/CompanyType.enum';

export class CompanyDTO {

  name: string;
  summery: string;
  desription: string;
  type: CompanyType;

  constructor(name: string, summery: string, description: string, type: CompanyType) {
     this.name = name;
     this.summery = summery;
     this.desription = description;
     this.type = type;
  }
}