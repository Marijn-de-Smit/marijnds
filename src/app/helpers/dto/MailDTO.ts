import { SubjectType } from '../enums/SubjectType.enum';

export class MailDTO {

  name: string;
  mail: string;
  phone: string;
  subject: SubjectType;
  message: string;

  constructor(name: string, mail: string, phone: string, subject: SubjectType, message: string) {
    this.name = name;
    this.mail = mail;
    this.phone = phone;
    this.subject = subject;
    this.message = message;
  }
}
