import { ProjectType } from '../enums/ProjectType.enum';

export class ProjectDTO {

  name: string;
  summery: string;
  desription: string;
  type: ProjectType;

  constructor(name: string, summery: string, description: string, type: ProjectType) {
     this.name = name;
     this.summery = summery;
     this.desription = description;
     this.type = type;
  }
}