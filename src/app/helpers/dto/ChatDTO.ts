export class ChatDTO {

  message: string;

  constructor(message: string) {
    this.message = message;
  }
}