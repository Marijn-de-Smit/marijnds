export class UserDTO {

  name: string;
  mail: string;
  phone: string;
  password: string;

  constructor(name: string, mail: string, phone: string, password: string) {
     this.name = name;
     this.mail = mail;
     this.phone = phone;
     this.password = password;
  }
}