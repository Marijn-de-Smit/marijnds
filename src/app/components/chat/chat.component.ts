import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatDTO } from 'src/app/helpers/dto/ChatDTO';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  model: any = {};
  Message = "";
  ErrorMessage = "";

  constructor(
    private router: Router,
    private contactService: ContactService
  ) {}

  ngOnInit(): void {

  }

  send() {
    let dto = new ChatDTO(this.model.message);
    this.contactService.sendMessage(dto)
      .subscribe(
        data => {
          if (data != null) {
            this.Message = "Message send succesfully!";
          } else {
            this.ErrorMessage = "Something went wrong!";
          }
        }
      );
  }
}