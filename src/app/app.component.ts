import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Marijnds';
  ngOnInit(): void {
    $(document).ready(function(){

      let page_elements = $('section');
      const view = $(window);

      // Actieve pagina bijhouden (Meeste is onepage, dus misschien per pagina doen)
      const url = $(location).attr('pathname');
      console.log(url);
      $('header a[href="'+ url +'"]').addClass('active');
    
      // Positie controleren
      isTop();
      ifInView();
    
      // De scroll animaties
      view.scroll( function() {  
        ifInView();
        isTop();
      });
    
      // Smooth scroll
      $(document).on('click', '.smooth-scroll a[href^="#"], a.smooth-scroll', function (event) {
        event.preventDefault();
    
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 60
        }, 800);
      });
    
      // Menu toggle
      $(".burger").click(function(){
    
        var burger = $(this);
        var menu = burger.parent();
    
        if(menu.hasClass("active")){
          menu.removeClass("active");
        }else{
          menu.addClass("active");
        }
      });
    
      function isTop(){
        if ($(document).scrollTop() >= 1) {
          $("header").addClass("scroll");
        } else {
          $("header").removeClass("scroll");
        }
      }
    
      function ifInView(){
        var window_height = view.height();
        var window_top_position = view.scrollTop();
        var window_bottom_position = (window_top_position + window_height);   

        $.each(page_elements, function() {
          var element = $(this);
          var element_height = element.outerHeight();
          var element_top_position = element.offset().top;
          var element_bottom_position = (element_top_position + element_height);
      
          if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
            let page = $("section.active").attr('id');
            if(page){
              $('header a').removeClass('active');
              $('header a[href="#'+ page +'"]').addClass('active');
            }

            page_elements.removeClass("active");
            element.addClass('active');
          }
        });
      };
    });
  }
}
