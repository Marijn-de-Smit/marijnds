import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {Restdata} from "src/app/helpers/restdata";
import { catchError } from 'rxjs/operators';
import { Util } from 'src/app/helpers/util';
import { UserDTO } from '../helpers/dto/UserDTO';
import { CompanyDTO } from '../helpers/dto/CompanyDTO';
import { ProjectDTO } from '../helpers/dto/ProjectDTO';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
  )
}

@Injectable()
export class CRUDService {

  redirectUrl: string;

  constructor(private http: HttpClient, private router: Router, private restData: Restdata) {
  }

  private handleError(error: Response) {
    return throwError(Util.createErrorMessage(error));
  }

  //users
  public createUser(dto: UserDTO) {
    return (this.http.post<any>(this.restData.getUrl('crudservice/user'), dto,
    )).pipe(catchError(this.handleError));
  }

  //companies
  public createCompany(dto: CompanyDTO) {
    return (this.http.post<any>(this.restData.getUrl('crudservice/company'), dto,
    )).pipe(catchError(this.handleError));
  }

  //projects
  public createProject(dto: ProjectDTO) {
    return (this.http.post<any>(this.restData.getUrl('crudservice/project'), dto,
    )).pipe(catchError(this.handleError));
  }
}
