import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {Restdata} from "src/app/helpers/restdata";
import { catchError } from 'rxjs/operators';
import { Util } from 'src/app/helpers/util';
import { MailDTO } from '../helpers/dto/MailDTO';
import { ChatDTO } from '../helpers/dto/ChatDTO';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
  )
}

@Injectable()
export class ContactService {
  
  redirectUrl: string;

  constructor(private http: HttpClient, private router: Router, private restData: Restdata) {
  }

  private handleError(error: Response) {
    return throwError(Util.createErrorMessage(error));
  }

  //mail
  public sendMail(dto: MailDTO) {
    return (this.http.post<any>(this.restData.getUrl('contactservice/mail'), dto,
    )).pipe(catchError(this.handleError));
  }

  //chat
  public sendMessage(dto: ChatDTO) {
    return (this.http.post<any>(this.restData.getUrl('contactservice/chat'), dto,
    )).pipe(catchError(this.handleError));
  }
}
