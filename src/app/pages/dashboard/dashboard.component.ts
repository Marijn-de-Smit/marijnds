import { Route } from '@angular/compiler/src/core';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { UserDTO } from 'src/app/helpers/dto/UserDTO';
import { CompanyDTO } from 'src/app/helpers/dto/CompanyDTO';
import { CompanyType } from 'src/app/helpers/enums/CompanyType.enum';
import { CRUDService } from 'src/app/services/crud.service';
import { ProjectDTO } from 'src/app/helpers/dto/ProjectDTO';
import * as $ from 'jquery';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  types = CompanyType;

  model: any = {};
  Message = "";
  ErrorMessage = "";

  constructor(
    private router: Router,
    private CRUDService: CRUDService
  ) {}

  ngOnInit(): void {
    $(document).ready(function() {
      let page_elements = $('section');
      const view = $(window);

      ifInView();
  
      view.scroll( function() {  
        ifInView();
      });

      $(document).on('click', '.smooth-scroll a[href^="#"], a.smooth-scroll', function (event) {
        event.preventDefault();
    
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 60
        }, 800);
      });

      function ifInView(){
        var window_height = view.height();
        var window_top_position = view.scrollTop();
        var window_bottom_position = (window_top_position + window_height);   

        $.each(page_elements, function() {
          var element = $(this);
          var element_height = element.outerHeight();
          var element_top_position = element.offset().top;
          var element_bottom_position = (element_top_position + element_height);
      
          if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
            let page = $("section.active").attr('id');
            if(page){
              $('header a').removeClass('active');
              $('header a[href="#'+ page +'"]').addClass('active');
            }

            page_elements.removeClass("active");
            element.addClass('active');
          }
        });
      };
    });
  }

  createUser() {
    let dto = new UserDTO(this.model.name, this.model.mail, this.model.phone, this.model.password);
    this.CRUDService.createUser(dto)
      .subscribe(
        data => {
          if (data != null) {
            this.Message = "User created succesfully!";
          } else {
            this.ErrorMessage = "Something went wrong!";
          }
        }
      );
  }

  createCompany() {
    let dto = new CompanyDTO(this.model.name, this.model.summery, this.model.desription, this.model.type);
    this.CRUDService.createCompany(dto)
      .subscribe(
        data => {
          if (data != null) {
            this.Message = "Company created succesfully!";
          } else {
            this.ErrorMessage = "Something went wrong!";
          }
        }
      );
  }

  createProject() {
    let dto = new ProjectDTO(this.model.name, this.model.summery, this.model.desription,this.model.type);
    this.CRUDService.createProject(dto)
      .subscribe(
        data => {
          if (data != null) {
            this.Message = "Project created succesfully!";
          } else {
            this.ErrorMessage = "Something went wrong!";
          }
        }
      );
  }
}

@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
  transform(data: Object) {
    return Object.keys(data);
  }
}