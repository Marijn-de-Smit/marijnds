import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { L10N_CONFIG, L10nConfig, L10N_LOCALE, L10nLocale, L10nTranslationService } from "angular-l10n";
import * as $ from 'jquery';
import '../../../lib/slick/slick.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  schema = this.l10nConfig.schema;
  title: string;

  model: any = {};

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    @Inject(L10N_CONFIG) private l10nConfig: L10nConfig,
    private translation: L10nTranslationService,

    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.translation.onChange().subscribe({
      next: (locale: L10nLocale) => {
        this.title = this.translation.translate("home.title");
      }
    });

    $(document).ready(function() {

      $('.context').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        dots: true,
        adaptiveHeight: true,
        asNavFor: '.preview'
      });
      $('.preview').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        asNavFor: '.context'
      });
    });
  }

  setLocale(locale: L10nLocale): void {
    this.translation.setLocale(locale);
  }
}
