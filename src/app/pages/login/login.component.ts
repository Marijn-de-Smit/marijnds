import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationDTO } from 'src/app/helpers/dto/AuthenticationDTO';
import { AuthenticationService } from 'src/app/services/authentication.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  errorMessage: string[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void { }

  login() {
    let dto = new AuthenticationDTO(this.model.mail, this.model.password);
    this.authenticationService.login(dto)
    .subscribe(
      data => {
        if(data != null) {
          this.setStorageData(data);
        }else{
          this.errorMessage.push("The server was't able to retreive your account");
        }
      },
      error => {
          this.errorMessage.push("Incorrect e-mail or password entered");          
      });
  }

  setStorageData(data: any){
    localStorage.setItem('token', data.entity.token);
    localStorage.setItem("accountRole", data.entity.role);
    this.router.navigateByUrl('/dashboard'); 
  }

}
