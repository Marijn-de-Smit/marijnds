import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { MailDTO } from 'src/app/helpers/dto/MailDTO';
import { SubjectType } from 'src/app/helpers/enums/SubjectType.enum';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  subjects = SubjectType;

  model: any = {};
  Message = "";
  ErrorMessage = "";

  constructor(
    private router: Router,
    private contactService: ContactService
  ) {}

  ngOnInit(): void {

  }

  send() {
    let dto = new MailDTO(this.model.name, this.model.mail, this.model.phone, this.model.subject, this.model.message);
    this.contactService.sendMessage(dto)
      .subscribe(
        data => {
          if (data != null) {
            this.Message = "Message send succesfully!";
          } else {
            this.ErrorMessage = "Something went wrong!";
          }
        }
      );
  }
}

@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
  transform(data: Object) {
    return Object.keys(data);
  }
}