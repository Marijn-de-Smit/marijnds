import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectBlackmirrorComponent } from './project-blackmirror.component';

describe('ProjectBlackmirrorComponent', () => {
  let component: ProjectBlackmirrorComponent;
  let fixture: ComponentFixture<ProjectBlackmirrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectBlackmirrorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectBlackmirrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
