import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDoodlesComponent } from './project-doodles.component';

describe('ProjectDoodlesComponent', () => {
  let component: ProjectDoodlesComponent;
  let fixture: ComponentFixture<ProjectDoodlesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectDoodlesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDoodlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
