import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import '../../../lib/slick/slick.js';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    $(document).ready(function() {

      $('.context').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        dots: true,
        adaptiveHeight: true,
        asNavFor: '.preview'
      });
      $('.preview').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        asNavFor: '.context'
      });
    });
  }

}
