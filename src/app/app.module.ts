import { NgModule, APP_INITIALIZER } from '@angular/core';

//imports
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule } from "@angular/material/form-field";
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatChipsModule} from '@angular/material/chips';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatRadioModule} from '@angular/material/radio';
import {MatSliderModule} from '@angular/material/slider';

import { L10nConfig, L10nLoader, L10nTranslationModule, L10nIntlModule } from "angular-l10n";
import { l10nConfig, initL10n } from "./l10n-config";

//components
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { PanelComponent } from './components/panel/panel.component';
import { LogoComponent } from './components/logo/logo.component';
import { CookiesComponent } from './components/cookies/cookies.component';
import { ChatComponent } from './components/chat/chat.component';
import { ModalComponent } from './components/modal/modal.component';
import { PopUpComponent } from './components/pop-up/pop-up.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';

//pages
import { AppComponent } from './app.component';
import { ErrorComponent } from './pages/error/error.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { PolicyComponent } from './pages/policy/policy.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ProjectComponent } from './pages/project/project.component';
import { CompaniesComponent } from './pages/companies/companies.component';
import { CompanyComponent } from './pages/company/company.component';
import { ContactComponent, EnumToArrayPipe } from './pages/contact/contact.component';
import { TimelineComponent } from './pages/timeline/timeline.component';
import { DonationComponent } from './pages/donation/donation.component';
import { ThanksComponent } from './pages/thanks/thanks.component';
import { ProjectDoodlesComponent } from './pages/project/specials/project-doodles/project-doodles.component';
import { ProjectBlackmirrorComponent } from './pages/project/specials//project-blackmirror/project-blackmirror.component';
import { BlogComponent } from './pages/blog/blog.component';
import { CvComponent } from './pages/cv/cv.component';

//services
import { ContactService } from './services/contact.service';
import { AuthenticationService } from './services/authentication.service';
import { Restdata } from './helpers/restdata';
import { AuthGuardService } from './services/authguard.service';
import { CRUDService } from './services/crud.service';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    HomeComponent,
    LoginComponent,
    PolicyComponent,
    DashboardComponent,
    ProjectsComponent,
    ProjectComponent,
    HeaderComponent,
    FooterComponent,
    PanelComponent,
    LogoComponent,
    CompaniesComponent,
    CookiesComponent,
    ContactComponent,
    TimelineComponent,
    ChatComponent,
    DonationComponent,
    ThanksComponent,
    EnumToArrayPipe,
    ProjectDoodlesComponent,
    ProjectBlackmirrorComponent,
    BlogComponent,
    CvComponent,
    ModalComponent,
    PopUpComponent,
    TooltipComponent,
    CompanyComponent
  ],
  imports: [
    L10nTranslationModule.forRoot(l10nConfig),
    L10nIntlModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatIconModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTabsModule,
    MatChipsModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSliderModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initL10n,
      deps: [L10nLoader],
      multi: true
    },
    AuthenticationService,
    ContactService,
    Restdata,
    AuthGuardService,
    CRUDService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
