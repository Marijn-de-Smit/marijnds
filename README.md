TODO / IDEAS
- Mijn als illustratie op de landing
  - Alle kleuren hierin verwerken van de bedrijven waarvoor ik gewerkt heb
  - XML sitemap maken als site live staat
  - Google pagespeed
  - Op 21/11 heb ik mijn Docker verwijderd, later controleren en nieuw account maken.
  - Easeter eggs verzinnen
  - EnumToArray pipe global maken?
  - Zorgen dat je content aan modal kan meegeven en interacties terug gaan naar parent
  - Kijken of lib verwerkt kan worden binnen Angular zelf